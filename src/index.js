import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Game } from './components/Game';
import * as serviceWorker from './serviceWorker';

const root: ?Element = document.getElementById('root');

if (root != null) {
    ReactDOM.render(<Game />, root);
    serviceWorker.unregister();
}
